# README #

Sh4dowCode - Subscribe to Download

### What is this repository for? ###

* An easy way to have a Sub to Donwload Page on your Website.
* Version v2.0

### How do I get set up? ###

* Import the js file with ```<script src="path/to/the/script/std.js"></script>```
* Have followeing code beload the Script, and change the variables
```javascript
<script>
	CHAID_p1 = "CHANAL ID Chanal 1";
	CHAID_p2 = "CHANAL ID Chanal 2";
	CHAID_p3 = "CHANAL ID Chanal 3";
	finPage = "Redirect Page"
	NAME_p1 = "Name of Chanal 1";
	NAME_p2 = "Name of Chanal 2";
	NAME_p3 = "Name of Chanal 3";
</script>
```
* Use following Buttons, and add a custom class if needed.
```html
<button onclick="subp1()" id="p1">P1</button>
<button onclick="subp2()" id="p2">P2</button>
<button onclick="subp3()" id="p3">P3</button>
<button onclick="subcheck()">Check</button>
```
* Add following Script over the </body> tag but under the Buttons.
```javascript
<script>
	document.getElementById("p1").innerHTML = NAME_p1;
	document.getElementById("p2").innerHTML = NAME_p2;
	document.getElementById("p3").innerHTML = NAME_p3;
</script>
```


### Who do I talk to? ###

* Sh4dowCode (https://reddit.com/u/Sh4dowCode)