var CHAID_p1;
var CHAID_p2;
var CHAID_p3;

var NAME_p1 = "P1";
var NAME_p2 = "P2";
var NAME_p3 = "P3";

var finPage = "https://example.com"

var subed_p1 = false;
var subed_p2 = false;
var subed_p3 = false;

function subp1() {
	if(!subed_p1)openInNewTab("https://www.youtube.com/channel/" + CHAID_p1 + "?sub_confirmation=1");
	if(subed_p1)alert("You have allready subscribed to "+ NAME_p1 +"!");
	subed_p1 = true;
}
											
function subp2() {
	if(!subed_p2)openInNewTab("https://www.youtube.com/channel/" + CHAID_p2 + "?sub_confirmation=1");
	if(subed_p2)alert("You have allready subscribed to "+ NAME_p2 +"!");
	subed_p2 = true;
}
											
function subp3() {
	if(!subed_p3)openInNewTab("https://www.youtube.com/channel/" + CHAID_p3 + "?sub_confirmation=1");
	if(subed_p3)alert("You have allready subscribed to "+ NAME_p3 +"!");
	subed_p3 = true;
}
											
function subcheck() {
	if(subed_p1 && subed_p2 && subed_p3) {
		openTab(finPage);
	} else {
		alert("You didn't subscribe to everyone.");
	}
}

function openInNewTab(url) {
	var win = window.open(url, '_blank');
	win.focus();
	}

	function openTab(url) {
	var win = window.location.href = (url);
	win.focus();
}